Changes
-------

0.4 (2015-09-25)
~~~~~~~~~~~~~~~~

- Packaging tweaks


0.3 (2015-09-23)
~~~~~~~~~~~~~~~~

- Support Python 3.5 asynchronous context managers


0.2 (2015-09-09)
~~~~~~~~~~~~~~~~

- First (usable) distribution on PyPI


0.1 (private)
~~~~~~~~~~~~~

Works reasonably well!


0.0 (private)
~~~~~~~~~~~~~

Initial effort.
